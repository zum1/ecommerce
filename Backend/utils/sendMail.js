const nodeMailer = require('nodemailer')

const sendMail = async(option)=>{
    const transporter = nodeMailer.createTransport({
        service:process.env.SMTP_SERVICE,
        auth:{
            user:process.env.SMPT_MAIL,
            pass:process.env.SMPT_PASSWORD
        }
    })

    const mailOptions = {
        from:process.env.SMPT_MAIL,
        to: options.email,
        text: options.message
    }

    transporter.sendMail(mailOptions)
}

module.exports=sendMail