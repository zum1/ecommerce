// creating token and saving in cookie

const sendToken = (user,statuscode,res)=>{
    const token = user.generateJwtToken()

    const options={
        expiresIn: new Date(Date.now()+1000*60*60*24),
        httpOnly:true
    }

    res.status(statuscode).cookie('token',token,options).json(
        {
            success:true,
            user,
            token
        }
    )

}

module.exports = sendToken