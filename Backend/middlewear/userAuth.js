//getting a token from a cookie and verifying user

const jwt = require('jsonwebtoken')
const User = require('../models/user.model')
const ErrorHandler = require('../utils/errorHandler')
const catchAsyncerrors = require('./catchAsyncerrors')
exports.userAuth = catchAsyncerrors(async (req, res, next) => {
  const {token} = req.cookies
  if (!token) {
    return next(new ErrorHandler('UnAuthorized User'), 401)
  }
  const isMatched = jwt.verify(token, process.env.JWT_SECRET)
  req.user = await User.findById(isMatched.id)
  next()

})

exports.roleAuth = (...roles)=>{
  return (req,res,next)=>{
    if(!roles.includes(req.user.role)){
      return next(new ErrorHandler('you are not an Admin'),403)
    }
    next()
  }
}