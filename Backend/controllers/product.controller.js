const Product=require('../models/product.model')
const ErrorHandler = require('../utils/errorHandler')
const AsyncErrors = require('../middlewear/catchAsyncerrors')

exports.getAllproducts=async (req,res,next)=>{
const {query:{name,price,sortBy='name',orderBy='desc'}}=req
const paging = extractPaging(req.query)
const query = constructQuery({name,price})
const { index, size: limit } = paging;
        const sort = {};
        if (req.query.sortBy && req.query.orderBy) {
            sort[req.query.sortBy] = req.query.orderBy === 'desc' ? -1 : 1;
        }
        const skip = index * limit;

const items = await Product.find(query, undefined, { limit, skip }).sort(sort)
if(!items){
    next(new ErrorHandler('product not found',404))
}
res.status(200).json(items)
}

const constructQuery = (condition)=>{
    const query ={}
    if(Object.keys(condition)){
        if (condition.name) {
            query.name = new RegExp(condition.name, 'i')
        }
        if(condition.price){
            query.price= { $lte: condition.price }
        }
    }
    return query
}

const extractPaging=(object)=> {
    const { index, size } = object;
    let parsedIndex = parseInt(index);
    let parsedSize = parseInt(size);
    if (!parsedIndex || parsedIndex < 1 || isNaN(parsedIndex)) parsedIndex = 0;
    else parsedIndex -= 1;
    if (!parsedSize || parsedSize < 1 || isNaN(parsedSize)) parsedSize = 10;

    return {
        index: parsedIndex,
        size: parsedSize
    }
}

exports.createProduct= AsyncErrors( async  (req,res,next)=>{
    req.body.user=req.user._id
    const item = await Product.create(req.body)
    if(!item){
        return next(new ErrorHandler('item not saved',404))
    }
    res.status(200).json(item)
})

exports.updateProduct = AsyncErrors,async (req,res,next)=>{
   const _id=req.params.id
   const isproduct = await Product.findById(_id)
   if(!isproduct){
       return next(new ErrorHandler('product not found',404))
   }
    const updatedItem = await Product.findByIdAndUpdate({ _id }, req.body);
    res.status(200).json({updatedItem})
}

exports.deleteProduct=AsyncErrors,async(req,res,next)=>{
   const _id=req.params.id;
   const isproduct = await Product.findById(_id)
   if(!isproduct){
       return next(new ErrorHandler('product not found',405))
   }
   const deltedItem = await Product.deleteOne({_id})
   res.status(200).json({deltedItem})
}

exports.setReviews=AsyncErrors(async(req,res,next)=>{
    

})