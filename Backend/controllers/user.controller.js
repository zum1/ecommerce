const User = require('../models/user.model') 
const ErrorHandler = require('../utils/errorHandler')
const AsyncErrors = require('../middlewear/catchAsyncerrors')
const bcrypt = require('bcryptjs')
const sendToken = require('../utils/jwtauth')
const catchAsyncerrors = require('../middlewear/catchAsyncerrors')

exports.getAlluser=async (req,res,next)=>{
const {query:{name,price,sortBy='name',orderBy='desc'}}=req
const paging = extractPaging(req.query)
const query = constructQuery({name,price})
const { index, size: limit } = paging;
        const sort = {};
        if (req.query.sortBy && req.query.orderBy) {
            sort[req.query.sortBy] = req.query.orderBy === 'desc' ? -1 : 1;
        }
        const skip = index * limit;

const items = await User.find(query, undefined, { limit, skip }).sort(sort)
if(!items){
    next(new ErrorHandler('User not found',404))
}
res.status(200).json(items)
}

const constructQuery = (condition)=>{
    const query ={}
    if(Object.keys(condition)){
        if (condition.name) {
            query.name = new RegExp(condition.name, 'i')
        }
        if(condition.price){
            query.price= { $lte: condition.price }
        }
    }
    return query
}

const extractPaging=(object)=> {
    const { index, size } = object;
    let parsedIndex = parseInt(index);
    let parsedSize = parseInt(size);
    if (!parsedIndex || parsedIndex < 1 || isNaN(parsedIndex)) parsedIndex = 0;
    else parsedIndex -= 1;
    if (!parsedSize || parsedSize < 1 || isNaN(parsedSize)) parsedSize = 10;

    return {
        index: parsedIndex,
        size: parsedSize
    }
}

exports.createUser= AsyncErrors( async  (req,res,next)=>{
    const user = await User.create(req.body)
    if(!user){
        return next(new ErrorHandler('user not saved',404))
    }
    let token = user.generateJwtToken();
    res.status(200).json(token)
})

exports.userLogin = AsyncErrors( async (req,res,next)=>{
    const {email,password}=req.body
    if(!email|| !password){
        return next(new ErrorHandler('please enter email and password',400))
    }
    const user = await User.findOne({email}).select('+password')

    if(!user){
        return next(new ErrorHandler('email id and password does not match',401))
    }

    const isPasswordMatch = await bcrypt.compare(password,user.password)

    if(!isPasswordMatch){
        return next(new ErrorHandler('email id and password does not match',401))
    }

   sendToken(user,200,res)

})

exports.logout=AsyncErrors(async(req,res,next)=>{

    res.cookie('token',null,{
        expiresIn: new Date(Date.now()),
        httpOnly:true
    })

    res.status(200).json({
        status:true,
        message:'Logged Out Successfully'
    })
})

exports.updateUser = AsyncErrors,async (req,res,next)=>{
   const _id=req.params.id
   const isUser = await User.findById(_id)
   if(!isUser){
       return next(new ErrorHandler('User not found',404))
   }
    const updatedItem = await User.findByIdAndUpdate({ _id }, req.body);
    res.status(200).json({updatedItem})
}

exports.deleteUser=AsyncErrors,async(req,res,next)=>{
   const _id=req.params.id;
   const isUser = await User.findById(_id)
   if(!isUser){
       return next(new ErrorHandler('User not found',405))
   }
   const deltedItem = await User.deleteOne({_id})
   res.status(200).json({deltedItem})
}

// reset password

exports.forgotPassword = catchAsyncerrors(async(req,res,next)=>{
    const user = await User.findOne({email:req.body.email})
    if(!user){
        return next(new ErrorHandler('user not found',404))
    }

    // get password token

    const resetToken = user.getResetPasswordToken()
    const savedUser=await user.save({validateBeforeSave:false})
    const resetPasswordUrl = `${req.protocol}://${req.get('host')}/api/v1/password/reset/${resetToken}`;
    const message=`your password reset token is :- \n\n ${resetPasswordUrl} \n\n if you have not registerd this email 
    please ignore`;

    try{
        await sendEmail({
            email:user.email,
            subject:'ecommerce recovery password',
            message,
        })
        res.status(200).json({
            success:true,
            message: `email sent to ${user.email} successfully`
        })
    }catch(error){
        user.getResetPasswordToken= undefined
        user.resetPasswordUrl=undefined

        await user.save()
        return next(new ErrorHandler(error.message,500))
    }

})

