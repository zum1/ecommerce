const express = require('express')
const cookieparser = require('cookie-parser')
const product=require('./routes/product.route')
const user=require('./routes/user.route')
const path= require('path')

require('dotenv').config({
    path: path.resolve('Backend/config/config.env'),
  });

const errorMiddlewear = require('./middlewear/error') 
const app=express()
app.use(cookieparser())
app.use(express.json())
//routes
app.use('/api/v1',product)
app.use('/api/v1',user)

// error middlewear
app.use(errorMiddlewear)

module.exports =app