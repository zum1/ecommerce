const express = require('express')
const { getAllproducts,createProduct,updateProduct,deleteProduct } = require('../controllers/product.controller')
const {userAuth,roleAuth} = require('../middlewear/userAuth')
const router=express.Router()

router.route('/products')
.get(getAllproducts)
.post(userAuth,roleAuth('admin'),createProduct)
router.route('/products/:id')
.put(userAuth,roleAuth('admin'),updateProduct)
.delete(userAuth,roleAuth('admin'),deleteProduct)
module.exports=router