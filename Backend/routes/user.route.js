const express = require('express')
const {getAlluser,createUser,updateUser,deleteUser,userLogin,logout, forgotPassword} = require('../controllers/user.controller')
const router=express.Router()

router.route('/user')
.post(createUser)
.get(getAlluser)
router.route('/login')
.post(userLogin)
router.route('/forgotpassword')
.post(forgotPassword)
router.route('/logout')
.get(logout)
router.route('/user/:id')
.put(updateUser)
.delete(deleteUser)

module.exports= router