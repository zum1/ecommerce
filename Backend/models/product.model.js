const mongoose=require('mongoose')

const productSchema= mongoose.Schema({
    name:{
        type:String,
        required:[true,'please enter product name'],
        trim:true
    },
    description:{
        type:String,
        required:[true,'please enter product description']
    },
    price:{
        type:Number,
        required:[true,'please enter product name'],
        maxLength:[4,'please enter upto 4 characters']
    },
    ratings:{
        type:Number,
        default:0
    },
    images:[
        {
            public_id:{
                type:String,
                required:true
            },
            url:{
                type:String,
                required:true
            }
        }
    ],
    category:{
        type:String,
        required:[true,'please enter product category']
    },
    stock:{
        type:Number,
        required:[true,'please enter stock'],
        maxLength:[4,'please enter upto 4 characters'],
        default:1
    },
    numofReviews:{
        type:Number,        
        default:0
    },

reviews:[
    {
        user:{
            type:mongoose.Types.ObjectId,
            ref:'User',
            required:true
        },
        name:{
            type:String,
            required:true
        },
        rating:{
            type:Number,
            required:true
        },
        comment:{
            type:String,
            required:true
        }
    }
],
createdAt:{
    type:Date,
    default:Date.now()
},
user:{
    type:mongoose.Types.ObjectId,
    ref:'User',
    required:true
}
})

const Product = mongoose.model('product',productSchema)

module.exports=Product;