const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const crypto = require('crypto')

const userSchema = new mongoose.Schema({
    name:{
        type:String,
        required:[true,'please enter name'],
        maxLength:[30,'name cannot be more then 30 characters'],
        minLength:[5,'name cannot be less then 5 characters']
    },
    email:{
        type:String,
        required:[true,'please enter name'],
         maxLength:[30,'name cannot be more then 30 characters'],
         unique:true,
         index:1,
         validate:[validator.isEmail,'please enter a valid email']

    },
    password:{
        type:String,
        required:[true,'please enter valid email'],
        minLength:[8,'password cannot be less then 8 characters'],
        select:false
    },
    avatar:{
         public_id:{
                type:String,
                required:true
            },
            url:{
                type:String,
                required:true
            }
    },

role:{
    type:String,
    default:'user'
},

resetPasswordToken:String,
resetPasswordExpire:Date

},{
    timestamps:true
})

userSchema.pre('save', async function(){
    if(!this.isModified('password')){
        next()
    }
    this.password = await bcrypt.hash(this.password,10)
})
// generate JWT token
userSchema.methods.generateJwtToken =  function(){
    return  jwt.sign({id:this._id}, process.env.JWT_SECRET,{
        expiresIn:process.env.JWT_EXPIRESIN
    })
}
// generating password reset token
userSchema.methods.getResetPasswordToken = function(){
    // generating token
    const resetToken = crypto.randomBytes(20).toString('hex')

    // Hashing and adding resetPasswordToken to username

    this.resetPasswordToken=crypto
    .createHash('sha256')
    .update(resetToken)
    .digest('hex')

    this.resetPasswordExpire = Date.now() + 15 *60*1000

    return resetToken
}
const User = mongoose.model('user',userSchema)

module.exports=User