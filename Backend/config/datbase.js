const mongoose = require('mongoose')
const DB=process.env.DB_URI
const connectDBase=()=>{
    mongoose.connect(DB).then((data)=>{
        console.log('database connected')
    }).catch((err)=>{
        console.log('error connecting to database'+ err)
    })
    
}

module.exports=connectDBase